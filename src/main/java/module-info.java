module com.example.bmical {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens com.example.bmical to javafx.fxml;
    exports com.example.bmical;
}