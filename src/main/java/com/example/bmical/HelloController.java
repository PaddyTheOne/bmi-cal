package com.example.bmical;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class HelloController implements Initializable {

    @FXML
    private TextField TextAge;

    @FXML
    private TextField TextWeight;

    @FXML
    private TextField TextHeight;

    @FXML
    private Text TextBMI;

    @FXML
    private Slider ScrollerSex;

    @FXML
    private Button ButtonCalculate;

    @FXML
    private Button ButtonClear;

    @FXML
    private ImageView Image;

    @FXML
    private Canvas Canvas;

    @FXML
    private Pane test;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/BMI+Chart.jpg");
        Image image = new Image(file.toURI().toString());
        Image.setImage(image);
    }

    @FXML
    protected void onButtonCalculatePressed(){

        if (TextWeight.getText().length()>0 && TextHeight.getText().length()>0){
            Person subject = new Person(TextAge.getText(),TextWeight.getText(),TextHeight.getText());

            Double BMI = Double.parseDouble(subject.getWeight())/(Math.pow(Double.parseDouble(subject.getHeight()),2)/10000);

            TextBMI.setText("BMI: "+BMI);

            drawShape(Canvas.getGraphicsContext2D(), Integer.parseInt(TextHeight.getText()), Integer.parseInt(TextWeight.getText()));
        }

    }

    @FXML
    protected void onButtonClearPressed(){
        Canvas.getGraphicsContext2D().clearRect(0,0,1000,1000);
        TextBMI.setText("");
        TextAge.setText("");
        TextHeight.setText("");
        TextWeight.setText("");
    }

    public void drawShape(GraphicsContext gc, int height, int weight){

        gc.clearRect(0,0,1000,1000);

        int weightLocationStart = 66;
        int heightLocationStart = 50;

        int weightLocationInterval=15;
        int heightLocationInterval=11;

        int weightMaxLocation=weightLocationInterval*24;
        int heightMaxLocation=heightLocationInterval*17;

        // 97.7 minus 45.5
        double weightMinToMax=52.2;
        // 193 minus 152.4
        double heightMinToMax=40.8;

        double weightDifferensFromStart = weight-45.5;
        double heightDifferensFromStart = height-152.5;


        int placementOfDotWeight=(int)((weightMaxLocation/weightMinToMax)*weightDifferensFromStart);
        int placementOfDotHeight=(int)((heightMaxLocation/heightMinToMax)*heightDifferensFromStart);


        gc.setFill(Color.BLACK);

        gc.fillOval(weightLocationStart+placementOfDotWeight,heightLocationStart+placementOfDotHeight,10,10);
    }



}