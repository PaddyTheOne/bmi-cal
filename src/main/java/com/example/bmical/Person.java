package com.example.bmical;

public class Person {

    public Person(String Age, String Weight, String Height){
        this.Age = Age;
        this.Weight = Weight;
        this.Height = Height;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    private String Age;

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    private String Weight;

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    private String Height;


}
